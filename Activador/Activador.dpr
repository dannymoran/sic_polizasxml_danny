program Activador;

uses
  Vcl.Forms,
  UPrincipal in 'UPrincipal.pas' {FPrincipal},
  ULicencia in 'ULicencia.pas',
  UEnvVars in 'UEnvVars.pas',
  WbemScripting_TLB in 'WbemScripting_TLB.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFPrincipal, FPrincipal);
  Application.Run;
end.
