﻿unit UPreferencias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,strutils,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, UCuentasCo,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.ComCtrls,Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc,
  Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls, Vcl.ExtCtrls;

type
  TFConfig = class(TForm)
    Label1: TLabel;
    txtCtaBancos: TEdit;
    btnGuardar: TButton;
    btnSalir: TButton;
    Conexion: TFDConnection;
    gbCuentas: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    gbVentas: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    txtVentas0credito: TEdit;
    txtVentas16Credito: TEdit;
    txtVentas0Contado: TEdit;
    txtVentas16Contado: TEdit;
    txtVentas8Credito: TEdit;
    txtVentas8Contado: TEdit;
    gbIVAClientes: TGroupBox;
    Label5: TLabel;
    Label11: TLabel;
    txtIVAPendiente: TEdit;
    txtIVAPagado: TEdit;
    txtDescuento: TEdit;
    txtCtaClientes: TEdit;
    txtCtaIEPS: TEdit;
    btnAsignarANuevos: TButton;
    btnAsignarATodos: TButton;
    qryClientes: TFDQuery;
    pcCuentasGenerales: TPageControl;
    tsClientes: TTabSheet;
    tsProveedores: TTabSheet;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    gbCompras: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    txtCompras0Credito: TEdit;
    txtCompras16Credito: TEdit;
    txtCompras0Contado: TEdit;
    txtCompras16Contado: TEdit;
    txtCompras8Credito: TEdit;
    txtCompras8Contado: TEdit;
    gbIVAProveedores: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    txtIVAPendienteProveedores: TEdit;
    txtIVAPagadoProveedores: TEdit;
    txtDescuentoProveedores: TEdit;
    txtCtaProveedores: TEdit;
    txtIEPSProveedores: TEdit;
    btnAsignaraProveedoresNuevos: TButton;
    btnAsignaraTodosProveedores: TButton;
    qryProveedores: TFDQuery;
    Terceros: TTabSheet;
    SubCuentas: TTabSheet;
    grdSubCuentas: TDBGrid;
    qrySubCuentas: TFDQuery;
    dsSubCuentas: TDataSource;
    qrySubCuentasTERCERO_CO_ID: TIntegerField;
    qrySubCuentasNOMBRE: TStringField;
    qrySubCuentasES_PROVEEDOR: TStringField;
    qrySubCuentasES_CLIENTE: TStringField;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    qrySubCuentasCUENTA_CLIENTE_ID: TStringField;
    qrySubCuentasCUENTA_PROVEEDOR_ID: TStringField;
    txtCliente: TEdit;
    txtProveedor: TEdit;
    btn_generar: TButton;
    dtp_ini: TDateTimePicker;
    dtp_fin: TDateTimePicker;
    Label22: TLabel;
    Label23: TLabel;
    btn_terceros: TButton;
    qryXML: TFDQuery;
    Label24: TLabel;
    RadioGroup1: TRadioGroup;
    procedure txtCtaBancosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnGuardarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtCtaClientesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtDescuentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtVentas0creditoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtVentas0ContadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtVentas16CreditoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtVentas16ContadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtVentas8CreditoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtVentas8ContadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtIVAPendienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtIVAPagadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtCtaIEPSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnAsignarANuevosClick(Sender: TObject);
    procedure btnAsignarATodosClick(Sender: TObject);
    procedure CargarCuentas;
    procedure txtCtaClientesExit(Sender: TObject);
    procedure txtDescuentoExit(Sender: TObject);
    procedure txtVentas0creditoExit(Sender: TObject);
    procedure txtVentas0ContadoExit(Sender: TObject);
    procedure txtVentas16CreditoExit(Sender: TObject);
    procedure txtVentas16ContadoExit(Sender: TObject);
    procedure txtVentas8CreditoExit(Sender: TObject);
    procedure txtVentas8ContadoExit(Sender: TObject);
    procedure txtIVAPendienteExit(Sender: TObject);
    procedure txtIVAPagadoExit(Sender: TObject);
    procedure txtCtaIEPSExit(Sender: TObject);
    procedure txtCtaProveedoresExit(Sender: TObject);
    procedure txtDescuentoProveedoresExit(Sender: TObject);
    procedure txtDescuentoProveedoresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtCompras0CreditoExit(Sender: TObject);
    procedure txtCompras16CreditoExit(Sender: TObject);
    procedure txtCompras0ContadoExit(Sender: TObject);
    procedure txtCompras16ContadoExit(Sender: TObject);
    procedure txtCompras8CreditoExit(Sender: TObject);
    procedure txtCompras8ContadoExit(Sender: TObject);
    procedure txtIVAPendienteProveedoresExit(Sender: TObject);
    procedure txtIVAPagadoProveedoresExit(Sender: TObject);
    procedure txtIEPSProveedoresExit(Sender: TObject);
    procedure txtCompras0CreditoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtCompras0ContadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtCompras16CreditoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtCompras16ContadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtCompras8CreditoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtCompras8ContadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtIVAPendienteProveedoresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtIVAPagadoProveedoresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtIEPSProveedoresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtCtaProveedoresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnAsignaraProveedoresNuevosClick(Sender: TObject);
    procedure btnAsignaraTodosProveedoresClick(Sender: TObject);
    procedure grdSubCuentasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure txtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtProveedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btn_generarClick(Sender: TObject);
    procedure btn_tercerosClick(Sender: TObject);
    procedure pcCuentasGeneralesChange(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConfig: TFConfig;

implementation

{$R *.dfm}

procedure TFConfig.cargarcuentas;
begin
  //CLIENTES
  txtCtaClientes.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cuenta_clientes_id from sic_cuentas_co_gen_clientes)');
  txtVentas0credito.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_ventas0_credito_id from sic_cuentas_co_gen_clientes)');
  txtVentas0Contado.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_ventas0_contado_id from sic_cuentas_co_gen_clientes)');
  txtVentas16credito.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_ventas16_credito_id from sic_cuentas_co_gen_clientes)');
  txtVentas16contado.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_ventas16_contado_id from sic_cuentas_co_gen_clientes)');
  txtVentas8credito.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_ventas8_credito_id from sic_cuentas_co_gen_clientes)');
  txtVentas8contado.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_ventas8_contado_id from sic_cuentas_co_gen_clientes)');
  txtIVAPendiente.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_iva_pendiente_id from sic_cuentas_co_gen_clientes)');
  txtIVAPagado.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_iva_pagado_id from sic_cuentas_co_gen_clientes)');
  txtDescuento.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_descuento_id from sic_cuentas_co_gen_clientes)');
  txtCtaIEPS.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_ieps_id from sic_cuentas_co_gen_clientes)');

  //PROVEEDORES
  txtCtaProveedores.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_proveedores_id from sic_cuentas_co_gen_proveedores)');
  txtcompras0credito.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_compras0_credito_id from sic_cuentas_co_gen_proveedores)');
  txtcompras0Contado.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_compras0_contado_id from sic_cuentas_co_gen_proveedores)');
  txtcompras16credito.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_compras16_credito_id from sic_cuentas_co_gen_proveedores)');
  txtcompras16contado.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_compras16_contado_id from sic_cuentas_co_gen_proveedores)');
  txtcompras8credito.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_compras8_credito_id from sic_cuentas_co_gen_proveedores)');
  txtcompras8contado.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_compras8_contado_id from sic_cuentas_co_gen_proveedores)');
  txtIVAPendienteProveedores.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_iva_pendiente_id from sic_cuentas_co_gen_proveedores)');
  txtIVAPagadoProveedores.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_iva_pagado_id from sic_cuentas_co_gen_proveedores)');
  txtDescuentoProveedores.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_descuento_id from sic_cuentas_co_gen_proveedores)');
  txtIEPSProveedores.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_ieps_id from sic_cuentas_co_gen_proveedores)');
end;





procedure TFConfig.grdSubCuentasDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
const IsChecked : array[Boolean] of Integer =
      (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
  DrawState: Integer;
  DrawRect: TRect;
begin
  //DIBUJA CHECKBOX SI ES S COLUMNA CLIENTE
    if (Column.Field.FieldName = DBCheckBox1.DataField) then
    begin
      DrawRect:=Rect;
      InflateRect(DrawRect,-1,-1);

      DrawState := ISChecked[Column.Field.AsString = 'S'];

      grdSubCuentas.Canvas.FillRect(Rect);
      DrawFrameControl(grdSubCuentas.Canvas.Handle, DrawRect,
                       DFC_BUTTON, DrawState);
    end;

      //DIBUJA CHECKBOX SI ES S COLUMNA RROVEEDOR
    if (Column.Field.FieldName = DBCheckBox2.DataField) then
    begin
      DrawRect:=Rect;
      InflateRect(DrawRect,-1,-1);

      DrawState := ISChecked[Column.Field.AsString = 'S'];

      grdSubCuentas.Canvas.FillRect(Rect);
      DrawFrameControl(grdSubCuentas.Canvas.Handle, DrawRect,
                       DFC_BUTTON, DrawState);
    end;

   //EDIT BOX EN LA COLUMNA DE CUENTA CLIENTE
    if Column.FieldName.ToUpper = 'CUENTA_CLIENTE_ID' then
  begin
    if (gdSelected in State) then
    begin
          if qrySubCuentasES_CLIENTE.Value='S' then
        begin
          with txtCliente do
          begin
            Left := Rect.Left + grdSubCuentas.Left + 38;
            Top := Rect.Top + grdSubCuentas.Top + 61;
            Height := Rect.Height;
            Width := Rect.Width;
            Color := grdSubCuentas.Canvas.Brush.Color;
            Visible := true;
             if Conexion.ExecSQLScalar('select count(cuenta_cliente_id) from sic_cuentas_terceros where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value])>0 then
           begin
           Text:=qrySubCuentasCUENTA_CLIENTE_ID.Value;
           end
           else
           Text:='';
          end;
        end;
    end;
  end;
          //EDIT BOX EN LA COLUMNA DE CUENTA PROVEEDOR
   if Column.FieldName.ToUpper = 'CUENTA_PROVEEDOR_ID' then
  begin
    if (gdSelected in State) then
    begin
      if qrySubCuentasES_PROVEEDOR.Value='S' then
        begin
          with txtProveedor do
          begin
            Left := Rect.Left + grdSubCuentas.Left + 38;
            Top := Rect.Top + grdSubCuentas.Top + 61;
            Height := Rect.Height;
            Width := Rect.Width;
            Color := grdSubCuentas.Canvas.Brush.Color;
            Visible := true;
           if Conexion.ExecSQLScalar('select count(cuenta_proveedor_id) from sic_cuentas_terceros where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value])>0 then
           begin
           Text:=qrySubCuentasCUENTA_PROVEEDOR_ID.Value;
           end
           else
           Text:='';
          end;
        end;
    end;
  end;

end;

procedure TFConfig.pcCuentasGeneralesChange(Sender: TObject);
begin
  if (pcCuentasGenerales.ActivePage<>SubCuentas) then
    begin
      txtCliente.Visible:=False;
      txtProveedor.Visible:=False;
    end;
end;

procedure TFConfig.RadioGroup1Click(Sender: TObject);
begin
  if RadioGroup1.ItemIndex=0 then
  begin
   qrySubCuentas.SQL.Text:='select t.tercero_co_id,t.nombre,t.es_proveedor,t.es_cliente,(select cuenta_pt from cuentas_co where cuenta_id=sct.cuenta_cliente_id) as cuenta_cliente_id,'+
        ' (select cuenta_pt from cuentas_co where cuenta_id=sct.cuenta_proveedor_id) as cuenta_proveedor_id from terceros_co t'+
        ' left join sic_cuentas_terceros sct on t.tercero_co_id=sct.tercero_co_id';
        qrySubCuentas.IndexFieldNames:='es_proveedor';
  end
  else
  begin
       qrySubCuentas.SQL.Text:='select t.tercero_co_id,t.nombre,t.es_proveedor,t.es_cliente,(select cuenta_pt from cuentas_co where cuenta_id=sct.cuenta_cliente_id) as cuenta_cliente_id,'+
        ' (select cuenta_pt from cuentas_co where cuenta_id=sct.cuenta_proveedor_id) as cuenta_proveedor_id from terceros_co t'+
        ' left join sic_cuentas_terceros sct on t.tercero_co_id=sct.tercero_co_id';
        qrySubCuentas.IndexFieldNames:='es_cliente';
  end;
  qrySubCuentas.Open();
  txtCliente.Visible:=false;
  txtProveedor.Visible:=false;
end;

procedure TFConfig.btnAsignarANuevosClick(Sender: TObject);
var
  cliente_id : integer;
  cta_ventas0_contado_id, cta_ventas0_credito_id,
  cta_ventas16_contado_id, cta_ventas16_credito_id,
  cta_ventas8_contado_id, cta_ventas8_credito_id,
  cta_iva_pendiente_id,cta_iva_pagado_id,cta_ctes_id, cta_dscto_id, cta_ieps_id, cambios : integer;
begin
  qryClientes.First;
  while not qryClientes.eof do
  begin
    cambios := 0;
    cliente_id := qryclientes.FieldByName('cliente_id').AsInteger;
    if Conexion.ExecSQLScalar('select count(*) from SIC_CUENTAS_CONTABLES_CLIENTES where cliente_id ='+inttostr(cliente_id))=0 then
    begin
      cta_ctes_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCtaClientes.Text]);
      cta_dscto_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtDescuento.Text]);
      cta_ventas0_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas0Contado.Text]);
      cta_ventas0_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas0Credito.Text]);
      cta_ventas16_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas16Contado.Text]);
      cta_ventas16_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas16Credito.Text]);
      cta_ventas8_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas8Contado.Text]);
      cta_ventas8_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas8Credito.Text]);

      cta_iva_pendiente_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPendiente.Text]);
      cta_iva_pagado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPagado.Text]);
      cta_ieps_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCtaIEPS.Text]);
       Conexion.ExecSQL('insert into sic_cuentas_contables_clientes values('+
          ':cliente_id, :cv0c,:cv0cr,:cv16c,:cv16cr,:cv8c,:cv8cr,:cipd,:cip,:cdid,:cieps)',
          [cliente_id,
          cta_ventas0_contado_id,cta_ventas0_credito_id,
          cta_ventas16_contado_id,cta_ventas16_credito_id,
          cta_ventas8_contado_id,cta_ventas8_credito_id,
          cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id]);
       
       cambios := cambios + 1;
    end;
    qryclientes.Next;
  end;
  showmessage('Proceso terminado ' + inttostr(cambios)+' Clientes modificados');
end;



procedure TFConfig.btnAsignaraProveedoresNuevosClick(Sender: TObject);
var
  proveedor_id : integer;
  cta_compras0_contado_id, cta_compras0_credito_id,
  cta_compras16_contado_id, cta_compras16_credito_id,
  cta_compras8_contado_id, cta_compras8_credito_id,
  cta_iva_pendiente_id,cta_iva_pagado_id,cta_proveedores_id, cta_dscto_id, cta_ieps_id, cambios : integer;
begin
  qryProveedores.First;
  while not qryProveedores.eof do
  begin
    cambios := 0;
    proveedor_id := qryproveedores.FieldByName('proveedor_id').AsInteger;
    if Conexion.ExecSQLScalar('select count(*) from SIC_CUENTAS_CO_PROVEEDORES where proveedor_id ='+inttostr(proveedor_id))=0 then
    begin
      cta_proveedores_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCtaProveedores.Text]);
      cta_dscto_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtDescuentoProveedores.Text]);
      cta_compras0_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCompras0Contado.Text]);
      cta_compras0_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCompras0Credito.Text]);
      cta_compras16_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCompras16Contado.Text]);
      cta_compras16_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCompras16Credito.Text]);
      cta_compras8_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCompras8Contado.Text]);
      cta_compras8_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCompras8Credito.Text]);

      cta_iva_pendiente_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPendienteProveedores.Text]);
      cta_iva_pagado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPagadoProveedores.Text]);
      cta_ieps_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIEPSProveedores.Text]);
       Conexion.ExecSQL('insert into sic_cuentas_co_proveedores values('+
          ':proveedor_id, :cv0c,:cv0cr,:cv16c,:cv16cr,:cv8c,:cv8cr,:cipd,:cip,:cdid,:cieps,:IGN)',
          [proveedor_id,
          cta_compras0_contado_id,cta_compras0_credito_id,
          cta_compras16_contado_id,cta_compras16_credito_id,
          cta_compras8_contado_id,cta_compras8_credito_id,
          cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id,'N']);
       
       cambios := cambios + 1;
    end;
    qryProveedores.Next;
  end;
  showmessage('Proceso terminado ' + inttostr(cambios)+' Proveedores modificados');
end;
procedure TFConfig.btnAsignarATodosClick(Sender: TObject);
var
  cliente_id : integer;
  cta_ventas0_contado_id, cta_ventas0_credito_id,
  cta_ventas16_contado_id, cta_ventas16_credito_id,
  cta_ventas8_contado_id, cta_ventas8_credito_id,
  cta_iva_pendiente_id,cta_iva_pagado_id,cta_ctes_id, cta_dscto_id, cta_ieps_id, cambios : integer;
begin
  cambios := 0;
  if MessageDlg('Esta seguro de sobreescribir todas las cuentas ya existentes?',mtConfirmation,mbYesNo,0) = mrYes then
  begin
    qryClientes.First;
    while not qryClientes.eof do
    begin
      cliente_id := qryclientes.FieldByName('cliente_id').AsInteger;
      cta_ctes_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCtaClientes.Text]);
      cta_dscto_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtDescuento.Text]);
      cta_ventas0_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas0Contado.Text]);
      cta_ventas0_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas0Credito.Text]);
      cta_ventas16_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas16Contado.Text]);
      cta_ventas16_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas16Credito.Text]);
      cta_ventas8_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas8Contado.Text]);
      cta_ventas8_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtVentas8Credito.Text]);

      cta_iva_pendiente_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPendiente.Text]);
      cta_iva_pagado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPagado.Text]);
      cta_ieps_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCtaIEPS.Text]);

      if Conexion.ExecSQLScalar('select count(*) from sic_cuentas_contables_clientes where cliente_id = :cid',[cliente_id]) > 0 then
      begin
        Conexion.ExecSQL('update sic_cuentas_contables_clientes set'+
        ' cta_ventas0_contado_id= :cv0c, cta_ventas0_credito_id=:cv0cr,'+
        ' cta_ventas16_contado_id= :cv16c, cta_ventas16_credito_id=:cv16cr,'+
        ' cta_ventas8_contado_id= :cv8c, cta_ventas8_credito_id=:cv8cr,'+
        ' cta_iva_pendiente_id=:cipd,cta_iva_pagado_id=:cip,'+
        ' cta_descuento_id = :cdsid, cta_ieps_id = :cieps'+
        ' where cliente_id = :cid',
        [cta_ventas0_contado_id,cta_ventas0_credito_id,
         cta_ventas16_contado_id,cta_ventas16_credito_id,
         cta_ventas8_contado_id,cta_ventas8_credito_id,
        cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id, cliente_id])
      end
      else
      begin
        Conexion.ExecSQL('insert into sic_cuentas_contables_clientes values('+
        ':cliente_id, :cv0c,:cv0cr,:cv16c,:cv16cr,:cv8c,:cv8cr,:cipd,:cip,:cdid,:cieps)',
        [cliente_id,
        cta_ventas0_contado_id,cta_ventas0_credito_id,
        cta_ventas16_contado_id,cta_ventas16_credito_id,
        cta_ventas8_contado_id,cta_ventas8_credito_id,
        cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id])
      end;
      
      cambios := cambios + 1;
      qryclientes.Next;
    end;
    showmessage('Proceso terminado ' + inttostr(cambios)+' Clientes modificados');
  end;
end;

procedure TFConfig.btnAsignaraTodosProveedoresClick(Sender: TObject);
var
  proveedor_id : integer;
  cta_compras0_contado_id, cta_compras0_credito_id,
  cta_compras16_contado_id, cta_compras16_credito_id,
  cta_compras8_contado_id, cta_compras8_credito_id,
  cta_iva_pendiente_id,cta_iva_pagado_id,cta_proveedores_id, cta_dscto_id, cta_ieps_id, cambios : integer;
begin
  cambios := 0;
  if MessageDlg('Esta seguro de sobreescribir todas las cuentas ya existentes?',mtConfirmation,mbYesNo,0) = mrYes then
  begin
    qryProveedores.First;
    while not qryProveedores.eof do
    begin
      proveedor_id := qryProveedores.FieldByName('proveedor_id').AsInteger;
      cta_proveedores_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCtaProveedores.Text]);
      cta_dscto_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtDescuentoProveedores.Text]);
      cta_compras0_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtcompras0Contado.Text]);
      cta_compras0_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtcompras0Credito.Text]);
      cta_compras16_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtcompras16Contado.Text]);
      cta_compras16_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtcompras16Credito.Text]);
      cta_compras8_contado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtcompras8Contado.Text]);
      cta_compras8_credito_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtcompras8Credito.Text]);

      cta_iva_pendiente_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPendienteProveedores.Text]);
      cta_iva_pagado_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIVAPagadoProveedores.Text]);
      cta_ieps_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtIEPSProveedores.Text]);

      if Conexion.ExecSQLScalar('select count(*) from sic_cuentas_co_proveedores where proveedor_id = :cid',[proveedor_id]) > 0 then
      begin
        Conexion.ExecSQL('update sic_cuentas_co_proveedores set'+
        ' cta_compras0_contado_id= :cv0c, cta_compras0_credito_id=:cv0cr,'+
        ' cta_compras16_contado_id= :cv16c, cta_compras16_credito_id=:cv16cr,'+
        ' cta_compras8_contado_id= :cv8c, cta_compras8_credito_id=:cv8cr,'+
        ' cta_iva_pendiente_id=:cipd,cta_iva_pagado_id=:cip,'+
        ' cta_descuento_id = :cdsid, cta_ieps_id = :cieps'+
        ' where proveedor_id = :cid',
        [cta_compras0_contado_id,cta_compras0_credito_id,
         cta_compras16_contado_id,cta_compras16_credito_id,
         cta_compras8_contado_id,cta_compras8_credito_id,
        cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id, proveedor_id])
      end
      else
      begin
        Conexion.ExecSQL('insert into sic_cuentas_co_proveedores values('+
        ':cliente_id, :cv0c,:cv0cr,:cv16c,:cv16cr,:cv8c,:cv8cr,:cipd,:cip,:cdid,:cieps,:IGN)',
        [proveedor_id,
        cta_compras0_contado_id,cta_compras0_credito_id,
        cta_compras16_contado_id,cta_compras16_credito_id,
        cta_compras8_contado_id,cta_compras8_credito_id,
        cta_iva_pendiente_id,cta_iva_pagado_id,cta_dscto_id,cta_ieps_id,'N'])
      end;
      
      cambios := cambios + 1;
      qryProveedores.Next;
    end;
    showmessage('Proceso terminado ' + inttostr(cambios)+' Proveedores modificados');
  end;
end;

procedure TFConfig.btnGuardarClick(Sender: TObject);
var
  cta_bancos_id : integer;
  cm : string;
begin
  cta_bancos_id := Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt = :cuenta',[txtCtaBancos.Text]);
  cm := 'update sic_plantillas_config set cta_bancos_general_id = '+inttostr(cta_bancos_id);
  Conexion.ExecSQL(cm);
  ModalResult := mrOk;
end;

procedure TFConfig.btn_generarClick(Sender: TObject);
var cuenta_id,cuenta_padre_id,creadas,sub_cuentas:integer;
nombre,cuenta_jt,cuenta_pt,ultimos_nueve:string;
begin
qrySubCuentas.First;
creadas:=0;
while not qrySubCuentas.Eof do
 begin
    //CUENTAS CLIENTES
      if (qrySubCuentasCUENTA_CLIENTE_ID.Value<>'') then
     begin
     cuenta_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[qrySubCuentasCUENTA_CLIENTE_ID.Value]);
     nombre:=qrySubCuentasNOMBRE.Value;
     nombre:=copy(nombre,0,50);
     //SI ES DE ULTIMO NIVEL CREAR LA PRIMER SUBCUENTA
     if Conexion.ExecSQLScalar('select num_subcuentas from get_num_subcuentas(:ctaid)',[cuenta_id])=0 then
      begin
        if Conexion.ExecSQLScalar('select count(nombre) from get_arbol_cuentas(:ctaid,0,''N'',''N'') where nombre=:nom',[cuenta_id,nombre])=0 then
         begin
          cuenta_padre_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[qrySubCuentasCUENTA_CLIENTE_ID.Value]);
          cuenta_jt:=Conexion.ExecSQLScalar('select cuenta_jt from get_info_cuenta(:ctaid,null)',[cuenta_id]);
          cuenta_jt:=cuenta_jt+'.000000001';
           Conexion.ExecSQL('insert into cuentas_co(cuenta_id,cuenta_padre_id,cuenta_pt,cuenta_jt,subcuenta,nombre,oculta,es_prorrateo)'+
           ' values(-1,:cpid,:cpt,:cjt,1,:nom,''N'',''N'')',[cuenta_padre_id,qrySubCuentasCUENTA_CLIENTE_ID.Value+'.1',cuenta_jt,nombre]);
          Conexion.Commit;
          creadas:=creadas+1;
         end;
      end
      else //SI NO CHECAR CUANTAS HAY Y CREAR ULTIMA
      begin
              if Conexion.ExecSQLScalar('select count(nombre) from get_arbol_cuentas(:ctaid,0,''N'',''N'') where nombre=:nom',[cuenta_id,nombre])=0 then
         begin
          cuenta_padre_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[qrySubCuentasCUENTA_CLIENTE_ID.Value]);
          cuenta_jt:=Conexion.ExecSQLScalar('select cuenta_jt from get_info_cuenta((select max(cuenta_id) from get_arbol_cuentas(:ctaid,0,''N'',''N'')),null)',[cuenta_id]);
          ultimos_nueve:=RightStr(cuenta_jt,9);
          ultimos_nueve:=inttostr(strtoint(ultimos_nueve)+1);
          ultimos_nueve:=Format('%.*d',[9, strtoint(ultimos_nueve)]);
          cuenta_jt:=copy(cuenta_jt,0,Length(cuenta_jt)-9);
          cuenta_jt:=cuenta_jt+ultimos_nueve;
          sub_cuentas:=Conexion.ExecSQLScalar('select num_subcuentas from get_num_subcuentas(:ctaid)',[cuenta_id]);
          cuenta_pt:=qrySubCuentasCUENTA_CLIENTE_ID.Value+'.'+inttostr(sub_cuentas+1);
           Conexion.ExecSQL('insert into cuentas_co(cuenta_id,cuenta_padre_id,cuenta_pt,cuenta_jt,subcuenta,nombre,oculta,es_prorrateo)'+
           ' values(-1,:cpid,:cpt,:cjt,:sub,:nom,''N'',''N'')',[cuenta_padre_id,cuenta_pt,cuenta_jt,inttostr(sub_cuentas+1),nombre]);
          Conexion.Commit;
          creadas:=creadas+1;
         end;
      end;
     end;

       //CUENTAS PROVEEDOR
      if (qrySubCuentasCUENTA_PROVEEDOR_ID.Value<>'') then
     begin
     cuenta_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[qrySubCuentasCUENTA_PROVEEDOR_ID.Value]);
     nombre:=qrySubCuentasNOMBRE.Value;
     nombre:=copy(nombre,0,50);
     //SI ES DE ULTIMO NIVEL CREAR LA PRIMER SUBCUENTA
     if Conexion.ExecSQLScalar('select num_subcuentas from get_num_subcuentas(:ctaid)',[cuenta_id])=0 then
      begin
        if Conexion.ExecSQLScalar('select count(nombre) from get_arbol_cuentas(:ctaid,0,''N'',''N'') where nombre=:nom',[cuenta_id,nombre])=0 then
         begin
          cuenta_padre_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[qrySubCuentasCUENTA_PROVEEDOR_ID.Value]);
          cuenta_jt:=Conexion.ExecSQLScalar('select cuenta_jt from get_info_cuenta(:ctaid,null)',[cuenta_id]);
          cuenta_jt:=cuenta_jt+'.000000001';
           Conexion.ExecSQL('insert into cuentas_co(cuenta_id,cuenta_padre_id,cuenta_pt,cuenta_jt,subcuenta,nombre,oculta,es_prorrateo)'+
           ' values(-1,:cpid,:cpt,:cjt,1,:nom,''N'',''N'')',[cuenta_padre_id,qrySubCuentasCUENTA_PROVEEDOR_ID.Value+'.1',cuenta_jt,nombre]);
          Conexion.Commit;
          creadas:=creadas+1;
         end;
      end
      else //SI NO CHECAR CUANTAS HAY Y CREAR ULTIMA
      begin
              if Conexion.ExecSQLScalar('select count(nombre) from get_arbol_cuentas(:ctaid,0,''N'',''N'') where nombre=:nom',[cuenta_id,nombre])=0 then
         begin
          cuenta_padre_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[qrySubCuentasCUENTA_PROVEEDOR_ID.Value]);
          cuenta_jt:=Conexion.ExecSQLScalar('select cuenta_jt from get_info_cuenta((select max(cuenta_id) from get_arbol_cuentas(:ctaid,0,''N'',''N'')),null)',[cuenta_id]);
          ultimos_nueve:=RightStr(cuenta_jt,9);
          ultimos_nueve:=inttostr(strtoint(ultimos_nueve)+1);
          ultimos_nueve:=Format('%.*d',[9, strtoint(ultimos_nueve)]);
          cuenta_jt:=copy(cuenta_jt,0,Length(cuenta_jt)-9);
          cuenta_jt:=cuenta_jt+ultimos_nueve;
          sub_cuentas:=Conexion.ExecSQLScalar('select num_subcuentas from get_num_subcuentas(:ctaid)',[cuenta_id]);
          cuenta_pt:=qrySubCuentasCUENTA_PROVEEDOR_ID.Value+'.'+inttostr(sub_cuentas+1);
           Conexion.ExecSQL('insert into cuentas_co(cuenta_id,cuenta_padre_id,cuenta_pt,cuenta_jt,subcuenta,nombre,oculta,es_prorrateo)'+
           ' values(-1,:cpid,:cpt,:cjt,:sub,:nom,''N'',''N'')',[cuenta_padre_id,cuenta_pt,cuenta_jt,inttostr(sub_cuentas+1),nombre]);
          Conexion.Commit;
          creadas:=creadas+1;
         end;
      end;
     end;
   qrySubCuentas.Next;
 end;
 ShowMessage(inttostr(creadas)+' Subcuentas creadas.');
end;

procedure TFConfig.btn_tercerosClick(Sender: TObject);
var
  xml,xmlnuevo,naturaleza,rfc_proveedor,nombre_proveedor,nombre_cliente,rfc_cliente: string;
  doc: IXMLDocument;
  cantidad_terceros:integer;
  comprobante: IXMLNode;
begin
  //RECORRE EL XML Y OBTIENE LOS DATOS
  qryXML.SQL.Text:='select xml,naturaleza,rfc,nombre from repositorio_cfdi where fecha between :F_INI and :F_FIN and version=''3.3'' and TIPO_DOCTO_MSP not like ''Recibo de nómina''';
  qryXML.ParamByName('F_INI').Value:=formatdatetime('mm/dd/yyyy',dtp_ini.Date);
  qryXML.ParamByName('F_FIN').Value:=formatdatetime('mm/dd/yyyy',dtp_fin.Date);
  qryXML.Open();
  qryXML.First;
  while not qryXML.Eof do
    begin
    xml := qryXML.FieldByName('xml').AsString;
    xmlnuevo:=xml.Replace('ü','u');
    doc := LoadXMLData(xmlnuevo);
    naturaleza:=qryXML.FieldByName('naturaleza').AsString;
    comprobante := doc.ChildNodes['cfdi:Comprobante'];
    // EN CASO DE QUE SEAN NULOS ASIGNAR RFC GENERAL Y NOMBRE XXXXXXXXX
    if comprobante.childnodes['cfdi:Receptor'].Attributes['Rfc']=Null then rfc_cliente:='XAXX010101000'
    else rfc_cliente := comprobante.childnodes['cfdi:Receptor'].Attributes['Rfc'];
    if comprobante.childnodes['cfdi:Receptor'].Attributes['Nombre']=Null then nombre_cliente:='XXXXXXXXX'
    else nombre_cliente:=comprobante.childnodes['cfdi:Receptor'].Attributes['Nombre'];
    if comprobante.childnodes['cfdi:Emisor'].Attributes['Rfc']=Null then rfc_proveedor:='XAXX010101000'
    else rfc_proveedor := comprobante.childnodes['cfdi:Emisor'].Attributes['Rfc'];
    if comprobante.childnodes['cfdi:Emisor'].Attributes['Nombre']=Null then nombre_proveedor:='XXXXXXXXX'
    else nombre_proveedor:=comprobante.childnodes['cfdi:Emisor'].Attributes['Nombre'];
       //VERIFICAR SI ES PROVEEDOR (NATURALEZA 'R')
      if naturaleza='R' then
      begin
        //VERIFICA SI YA EXISTE Y SI ES CLIENTE
       if (Conexion.ExecSQLScalar('select count(rfc) from terceros_co where rfc=:rfc',[rfc_proveedor])>0) and (Conexion.ExecSQLScalar('select es_cliente from terceros_co where rfc=:rfc',[rfc_proveedor])='S') then
        begin  //ASIGNAR SI A PROVEEDOR EN CASO DE QUE NO SEA
                 if (Conexion.ExecSQLScalar('select es_proveedor from terceros_co where rfc=:rfc',[rfc_proveedor])='N') then
          begin
          Conexion.ExecSQL('update terceros_co set es_proveedor=''S'' where rfc=:rfc',[rfc_proveedor]);
          cantidad_terceros:=cantidad_terceros+1;
          Conexion.Commit;
          end;
        end
        //SI ES PROVEEDOR Y SI YA EXISTE NO HACER NADA (SIGNIFICA QUE YA ES PROVEEDOR)
        else if (Conexion.ExecSQLScalar('select count(rfc) from terceros_co where rfc=:rfc',[rfc_proveedor])>0) then
          begin

          end else //SI NO EXISTE LO CREA COMO PROVEEDOR
            begin
              try
              Conexion.ExecSQL('insert into terceros_co(tercero_co_id,nombre,tipo,rfc,es_proveedor,es_cliente,es_contratante)'+
              ' values(-1,:nom,''04'',:rfc,''S'',''N'',''N'')',[nombre_proveedor,rfc_proveedor]);
              cantidad_terceros:=cantidad_terceros+1;
              Conexion.Commit;
              except
              nombre_proveedor:=nombre_proveedor+'$';
              Conexion.ExecSQL('insert into terceros_co(tercero_co_id,nombre,tipo,rfc,es_proveedor,es_cliente,es_contratante)'+
              ' values(-1,:nom,''04'',:rfc,''S'',''N'',''N'')',[nombre_proveedor,rfc_proveedor]);
              cantidad_terceros:=cantidad_terceros+1;
              Conexion.Commit;
              end;
            end;
      end else
      //SI ES CLIENTE (NATURALEZA 'E')
              //VERIFICA SI YA EXISTE Y SI ES PROVEEDOR
       if (Conexion.ExecSQLScalar('select count(rfc) from terceros_co where rfc=:rfc',[rfc_cliente])>0) and (Conexion.ExecSQLScalar('select es_proveedor from terceros_co where rfc=:rfc',[rfc_cliente])='S') then
        begin  //ASIGNAR SI A CLIENTE EN CASO DE QUE NO SEA
         if (Conexion.ExecSQLScalar('select es_cliente from terceros_co where rfc=:rfc',[rfc_cliente])='N') then
          begin
          Conexion.ExecSQL('update terceros_co set es_cliente=''S'' where rfc=:rfc',[rfc_cliente]);
          cantidad_terceros:=cantidad_terceros+1;
          Conexion.Commit;
          end;
        end
        //SI ES CLIENTE Y SI YA EXISTE NO HACER NADA (SIGNIFICA QUE YA ES PROVEEDOR)
        else if (Conexion.ExecSQLScalar('select count(rfc) from terceros_co where rfc=:rfc',[rfc_cliente])>0) then
          begin

          end else //SI NO EXISTE LO CREA COMO CLIENTE
            begin
             try
              Conexion.ExecSQL('insert into terceros_co(tercero_co_id,nombre,tipo,rfc,es_proveedor,es_cliente,es_contratante)'+
              ' values(-1,:nom,''04'',:rfc,''N'',''S'',''N'')',[nombre_cliente,rfc_cliente]);
              cantidad_terceros:=cantidad_terceros+1;
              Conexion.Commit;
              except
              nombre_cliente:=nombre_cliente+'$';
              Conexion.ExecSQL('insert into terceros_co(tercero_co_id,nombre,tipo,rfc,es_proveedor,es_cliente,es_contratante)'+
              ' values(-1,:nom,''04'',:rfc,''N'',''S'',''N'')',[nombre_cliente,rfc_cliente]);
              cantidad_terceros:=cantidad_terceros+1;
              Conexion.Commit;
              end;
            end;
    qryXML.Next;
    end;
    ShowMessage(inttostr(cantidad_terceros)+' Terceros agregados o modificados.');
end;

procedure TFConfig.FormShow(Sender: TObject);
begin
  txtCtaBancos.Text := Conexion.ExecSQLScalar('select cuenta_pt from cuentas_co where cuenta_id = (select cta_bancos_general_id from sic_plantillas_config)');
  CargarCuentas;
  qrysubcuentas.Open();
 DBCheckBox1.Visible := False;
 DBCheckBox1.Color := grdSubCuentas.Color;
 DBCheckBox1.Caption := '';
 DBCheckBox2.Visible := False;
 DBCheckBox2.Color := grdSubCuentas.Color;
 DBCheckBox2.Caption := '';
end;

procedure TFConfig.txtClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
  cuenta:string;
begin
    //F4 BUSCAR CUENTAS
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.Open;
    busca_cuentas.txtBuscar.Text :=  txtCliente.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCliente.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      //SI EXISTE LO ACTUALIZA
      if Conexion.ExecSQLScalar('select count(tercero_co_id) from sic_cuentas_terceros where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value])>0 then
        begin
        Conexion.ExecSQL('update sic_cuentas_terceros set cuenta_cliente_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCliente.Text+''') where tercero_co_id='+inttostr(qrySubCuentasTERCERO_CO_ID.Value)+'');
        end
        else  //SI NO, LO CREA
        begin
        Conexion.ExecSQL('insert into sic_cuentas_terceros(cta_ter_id,tercero_co_id,cuenta_cliente_id) values (-1,:coid,:cpid)',[qrySubCuentasTERCERO_CO_ID.Value,Conexion.ExecSQLScalar('SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCliente.Text+'''')]);
        end;
      Conexion.Commit;
      grdSubCuentas.DataSource.DataSet.Refresh;
    end;
  end;

  //F8 COPIAR AL RENGLON SIGUIENTE
    if Key=VK_F8 then
  begin
  cuenta:=txtCliente.Text;
  qrySubCuentas.Next;
  if qrySubCuentasES_CLIENTE.Value='S' then
       begin //SI EXISTE LO ACTUALIZA
       if Conexion.ExecSQLScalar('select count(tercero_co_id) from sic_cuentas_terceros where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value])>0 then
       begin
        Conexion.ExecSQL('update sic_cuentas_terceros set cuenta_cliente_id=:cid where tercero_co_id=:tid',[Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[cuenta]),qrySubCuentasTERCERO_CO_ID.Value]);
       end
       else  //SI NO, LO CREA
       begin
       Conexion.ExecSQL('insert into sic_cuentas_terceros(cta_ter_id,tercero_co_id,cuenta_cliente_id) values (-1,:coid,:cpid)',[qrySubCuentasTERCERO_CO_ID.Value,Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[cuenta])]);
       end;
    Conexion.Commit;
    txtCliente.Text:=cuenta;
    grdSubCuentas.DataSource.DataSet.Refresh;
    end;
  end;

  //DARLE ENTER SI ESTA VACIO ELIMINAR REGISTRO SOLO SI EXISTE
  if Key=VK_RETURN then
    begin
      if Conexion.ExecSQLScalar('select count(tercero_co_id) from sic_cuentas_terceros where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value])>0 then
      begin
        if txtCliente.Text='' then
        begin
        Conexion.ExecSQL('update sic_cuentas_terceros set cuenta_cliente_id = NULL where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value]);
        Conexion.Commit;
        grdSubCuentas.DataSource.DataSet.Refresh;
          qrySubCuentas.Open();
        end;
      end;
    end;
end;



procedure TFConfig.txtCompras0ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras0_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtCompras0ContadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCompras0Contado.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCompras0Contado.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras0_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtCompras0CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras0_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtCompras0CreditoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCompras0Credito.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCompras0Credito.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras0_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtCompras16ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras16_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtCompras16ContadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCompras16contado.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCompras16contado.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras16_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtCompras16CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras16_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtCompras16CreditoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCompras16Credito.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCompras16Credito.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras16_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtCompras8ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras8_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtCompras8ContadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCompras8contado.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCompras8contado.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras8_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtCompras8CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras8_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtCompras8CreditoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCompras8Credito.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCompras8Credito.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_compras8_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;
procedure TFConfig.txtCtaBancosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.Open;
    busca_cuentas.txtBuscar.Text :=  txtCtaBancos.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCtaBancos.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
    end;
  end;
end;
procedure TFConfig.txtCtaClientesExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cuenta_clientes_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;

end;

procedure TFConfig.txtCtaClientesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCtaClientes.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCtaClientes.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cuenta_clientes_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtCtaIEPSExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ieps_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtCtaIEPSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCtaIEPS.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCtaIEPS.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ieps_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaIEPS.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtCtaProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_proveedores_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtCtaProveedoresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCtaProveedores.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCtaProveedores.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_proveedores_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtDescuentoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_descuento_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtDescuentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtDescuento.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtDescuento.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_descuento_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtDescuento.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtDescuentoProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_descuento_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtDescuentoProveedoresKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtDescuentoProveedores.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtDescuentoProveedores.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_descuento_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtIVAPagado.Text+''')');;
    end;
  end;
end;

procedure TFConfig.txtIEPSProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_ieps_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtIEPSProveedoresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtIEPSProveedores.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtIEPSProveedores.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_ieps_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtIVAPagadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_iva_pagado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtIVAPagadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtIVAPagado.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtIVAPagado.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_iva_pagado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtIVAPagado.Text+''')');;
    end;
  end;
end;

procedure TFConfig.txtIVAPagadoProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_iva_pagado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;
procedure TFConfig.txtIVAPagadoProveedoresKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtIVAPagadoProveedores.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtIVAPagadoProveedores.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_iva_pagado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtIVAPendienteExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_iva_pendiente_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtIVAPendienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtIVAPendiente.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtIVAPendiente.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_iva_pendiente_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtIVAPendiente.Text+''')');;
    end;
  end;
end;

procedure TFConfig.txtIVAPendienteProveedoresExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_iva_pendiente_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtIVAPendienteProveedoresKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtIVAPendienteProveedores.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtIVAPendienteProveedores.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_proveedores set cta_iva_pendiente_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtCtaClientes.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtProveedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
  cuenta:string;
begin
  //F4 BUSCAR CUENTAS
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtProveedor.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtProveedor.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      //SI EXISTE LO ACTUALIZA
      if Conexion.ExecSQLScalar('select count(tercero_co_id) from sic_cuentas_terceros where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value])>0 then
        begin
        Conexion.ExecSQL('update sic_cuentas_terceros set cuenta_proveedor_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtProveedor.Text+''') where tercero_co_id='+inttostr(qrySubCuentasTERCERO_CO_ID.Value)+'');
        end
        else  //SI NO, LO CREA
        begin
        Conexion.ExecSQL('insert into sic_cuentas_terceros(cta_ter_id,tercero_co_id,cuenta_proveedor_id) values (-1,:coid,:cpid)',[qrySubCuentasTERCERO_CO_ID.Value,Conexion.ExecSQLScalar('SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtProveedor.Text+'''')]);
        end;
      Conexion.Commit;
      grdSubCuentas.DataSource.DataSet.Refresh;
    end;
  end;

    //F8 COPIAR AL RENGLON SIGUIENTE
    if Key=VK_F8 then
  begin
  cuenta:=txtProveedor.Text;
  qrySubCuentas.Next;
  if qrySubCuentasES_PROVEEDOR.Value='S' then
    begin //SI EXISTE LO ACTUALIZA
       if Conexion.ExecSQLScalar('select count(tercero_co_id) from sic_cuentas_terceros where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value])>0 then
       begin
        Conexion.ExecSQL('update sic_cuentas_terceros set cuenta_proveedor_id=:cid where tercero_co_id=:tid',[Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[cuenta]),qrySubCuentasTERCERO_CO_ID.Value]);
       end
       else  //SI NO, LO CREA
       begin
       Conexion.ExecSQL('insert into sic_cuentas_terceros(cta_ter_id,tercero_co_id,cuenta_proveedor_id) values (-1,:coid,:cpid)',[qrySubCuentasTERCERO_CO_ID.Value,Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[cuenta])]);
       end;
    Conexion.Commit;
    txtProveedor.Text:=cuenta;
    grdSubCuentas.DataSource.DataSet.Refresh;
    end;
  end;

    //DARLE ENTER SI ESTA VACIO ELIMINAR REGISTRO SOLO SI EXISTE
  if Key=VK_RETURN then
    begin
      if Conexion.ExecSQLScalar('select count(tercero_co_id) from sic_cuentas_terceros where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value])>0 then
      begin
        if txtProveedor.Text='' then
        begin
        Conexion.ExecSQL('update sic_cuentas_terceros set cuenta_proveedor_id = NULL where tercero_co_id=:id',[qrySubCuentasTERCERO_CO_ID.Value]);
        Conexion.Commit;
        grdSubCuentas.DataSource.DataSet.Refresh;
          qrySubCuentas.Open();
        end;
      end;
    end;
end;

procedure TFConfig.txtVentas0ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas0_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtVentas0ContadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtVentas0Contado.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtVentas0Contado.Text:= busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas0_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtVentas0contado.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtVentas0creditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas0_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtVentas0creditoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtVentas0credito.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtVentas0credito.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas0_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtVentas0credito.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtVentas16ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas16_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtVentas16ContadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtVentas16Contado.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtVentas16Contado.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas16_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtVentas16Contado.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtVentas16CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas16_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtVentas16CreditoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtVentas16Credito.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtVentas16Credito.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas16_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txtVentas16credito.Text+''')');
    end;
  end;
end;

procedure TFConfig.txtVentas8ContadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas8_contado_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtVentas8ContadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtVentas8Contado.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtVentas8Contado.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas8_contado_id = '+inttostr(busca_cuentas.cuenta_id));
    end;
  end;
end;

procedure TFConfig.txtVentas8CreditoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas8_credito_id = (SELECT CUENTA_ID FROm cuentas_co where cuenta_pt='''+txt.Text+''')');      
    end;
  end;
end;

procedure TFConfig.txtVentas8CreditoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtVentas8credito.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtVentas8credito.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      Conexion.ExecSQL('update sic_cuentas_co_gen_clientes set cta_ventas8_credito_id = '+inttostr(busca_cuentas.cuenta_id));
    end;
  end;
end;

end.
